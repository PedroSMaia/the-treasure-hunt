# Phaser 3 - The Treasure Hunt Game

Haze, um caçador de tesouros , conhecido pelas suas vastas descobertas, ouve falar da lenda de um tesouro perdido no mar. 
Muitos foram aqueles que tentaram encontrar este tesouro mas nunca bem sucedidos. Decidido a desvendar a localização secreta do tesouro,
começa à procura de pistas que o possam ajudar a encontrá-lo. 	Após um longo período de pesquisas consegue finalmente encontrar o tesouro.
Não sabia ele que este estava guardado por esqueletos de peixes, conhecidos como os Zandors, criaturas que protegiam este tesouro desde que o 
avio que o transportava naufragou.
Ajuda o Haze a recolher o maior número de moedas possíveis sem que os Zandors consigam impedi-lo!

## Requirements

[Node.js](https://nodejs.org) is required to install dependencies and run scripts via `npm`.

## Run Project

| Command | Description |
|---------|-------------|
| `npm install` | Install project dependencies |
| `npm start` | Build project and open web server running project |

