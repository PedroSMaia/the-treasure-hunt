import { Scene } from "phaser";
import Sky from './assets/images/sky.png';
import Ground from './assets/images/ground.png';

class PreloadScene extends Scene{
    constructor() {
        super('preload');
    }

    //Função para carregar a scene
    preload(){
        this.load.image('sky', Sky);
        this.load.image('ground', Ground);
    }

    //Função apra criar a scene
    create(){
        //Adiciona o background
        const sky = this.add.image(0, 0, 'sky');
        sky.setOrigin(0, 0);
        const ground = this.add.image(480, 580, 'ground');

        this.title = this.add.text(500, 250, 'The Treasure Hunt', {
            fontSize: '64px',
            fill: '#FFD700'
        });

        this.title.setOrigin(0.5);
        this.title.visible = true;

        this.initialText = this.add.text(500, 320, 'Game of the Year 2019', {
            fontSize: '40px',
            fill: '#fff'
        });

        this.initialText.setOrigin(0.5);
        this.initialText.visible = true;

        this.initialText2 = this.add.text(500, 370, 'Limited Edition', {
            fontSize: '35px',
            fill: '#FFD700'
        });

        this.initialText2.setOrigin(0.5);
        this.initialText2.visible = true;


        this.subText = this.add.text(500, 430, 'Press to start', {
            fontSize: '44px',
            fill: '#000'
        });

        this.subText.setOrigin(0.5);
        this.subText.visible = true;

        this.input.on('pointerdown', () => {
            this.scene.start('game');
        });
    }
}

export default PreloadScene