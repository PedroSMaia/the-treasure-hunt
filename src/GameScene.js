import {
    Scene
} from 'phaser';
import Sky from './assets/images/sky.png';
import Ground from './assets/images/ground.png';
import Plat1x1 from './assets/images/sand_1x1.png';
import Plat2x1 from './assets/images/sand_2x1.png';
import Plat4x1 from './assets/images/sand_4x1.png';
import Plat6x1 from './assets/images/sand_6x1.png';
import Plat8x1 from './assets/images/sand_8x1.png';
import Hero from './assets/images/hero.png';
import CoinIcon from './assets/images/coin_icon.png';
import CoinAnim from './assets/images/coin_animated.png';
import Enemies from './assets/images/fish.png';
import Decor from './assets/images/decor.png';
import particleConfig from './particleConfig';

class GameScene extends Scene {
    constructor() {
        super('game');
        this.score = 0;
        this.gameOver = false;
    }

    //Função para dar load da scene
    preload() {
        //Carrega as imagens
        this.load.image('sky', Sky);
        this.load.image('ground', Ground);
        this.load.image('plat1x1', Plat1x1);
        this.load.image('plat2x1', Plat2x1);
        this.load.image('plat4x1', Plat4x1);
        this.load.image('plat6x1', Plat6x1);
        this.load.image('plat8x1', Plat8x1);
        this.load.image('coinIcon', CoinIcon);

        //Carrega os Sprites
        this.load.spritesheet('coinAnim',
            CoinAnim, {
                frameWidth: 22,
                frameHeight: 22
            }
        );
        this.load.spritesheet('dude',
            Hero, {
                frameWidth: 35.5,
                frameHeight: 42
            }
        );
        this.load.spritesheet('enemies',
            Enemies, {
                frameWidth: 42,
                frameHeight: 32
            }
        );
        this.load.spritesheet('decorations',
            Decor, {
                frameWidth: 42,
                frameHeight: 42
            }
        );

        //Carrega o audio
        this.load.audio('audio_jump', 'src/assets/audio/jump.wav');
        this.load.audio('audio_coin', 'src/assets/audio/coin.wav');
        this.load.audio('audio_back', 'src/assets/audio/bgm.mp3');
        this.load.audio('audio_hitenemy', 'src/assets/audio/stomp.wav');
    }

    //Função para criar a scene
    create() {
        //Define o score como 0
        this.score = 0;
        //Adiciona o background
        const sky = this.add.image(0, 0, 'sky');
        sky.setOrigin(0, 0);

        this.soundBack = this.sound.add('audio_back');
        this.soundBack.play();
        this.soundBack.setLoop(true);

        //Chama as funções de criação
        this.createPlatform();
        this.createPlayer();
        this.createCursor();
        this.createCoins();
        this.createScore();
        this.createEnemies();
        this.createDecorations();
        this.createParticles();

        //Texto de Game Over
        this.gameOverText = this.add.text(500, 300, 'Game Over', {
            fontSize: '64px',
            fill: '#000'
        });
        this.gameOverText.depth = 100;
        this.gameOverText.setDepth(100);

        this.gameOverText.setOrigin(0.5);
        this.gameOverText.visible = false;

        //Texto de Final Score
        this.finalScoreText = this.add.text(500, 350, 'Score: ' + this.score, {
            fontSize: '64px',
            fill: '#FFD700'
        });
        this.finalScoreText.depth = 100;
        this.finalScoreText.setDepth(100);

        this.finalScoreText.setOrigin(0.5);
        this.finalScoreText.visible = false;
    }

    //Função para criar as particulas das moedas
    createParticles() {
        //Adiciona as particuals as moedas
        this.particles = this.add.particles('coinAnim');
        //Cria o emitter com as respectivas configurações das particulas
        this.emitter = this.particles.createEmitter(particleConfig);
    }

    //Função para criar as Plataformas
    createPlatform() {
        //Faz um random para determinar o nivel inicial
        this.level = Phaser.Math.Between(0, 3);

        //Cria um grupo com as propriedades fisicas defenidas e associa as plataformas
        this.platforms = this.physics.add.staticGroup();
        //Cria o ground do jogo
        this.platforms.create(480, 580, 'ground').setScale(1, 1).refreshBody();

        this.group = this.add.group();

        //Define um grupo estatico
        //this.decorations = this.physics.add.staticGroup();

        //Ciclo para determinar qual o nível que vai começar
        if (this.level == 0) {
            //Cria o ground e as plataformas do mapa

            this.platforms.create(480, 420, 'plat4x1');
            this.platforms.create(230, 200, 'plat2x1');
            this.platforms.create(420, 250, 'plat1x1');
            this.platforms.create(680, 300, 'plat4x1');

            //Adiciona as decorações ao mapa
            this.sprite1 = this.add.sprite(430, 380, 'decorations', [0]);
            this.sprite2 = this.add.sprite(460, 380, 'decorations', [4]);
            this.sprite3 = this.add.sprite(710, 262, 'decorations', [2]);
            this.sprite4 = this.add.sprite(740, 262, 'decorations', [3]);
            this.sprite5 = this.add.sprite(250, 160, 'decorations', [1]);
            //Adiciona ao grupo as decorações
            this.group.add(this.sprite1);
            this.group.add(this.sprite2);
            this.group.add(this.sprite3);
            this.group.add(this.sprite4);
            this.group.add(this.sprite5);

        } else if (this.level == 1) {
            //Cria o ground e as plataformas do mapa
            this.platforms.create(120, 420, 'plat6x1');
            this.platforms.create(410, 340, 'plat2x1');
            this.platforms.create(588, 517, 'plat1x1');
            this.platforms.create(176, 252, 'plat4x1');
            this.platforms.create(472, 188, 'plat4x1');
            this.platforms.create(778, 284, 'plat2x1');

            //Adiciona as decorações ao mapa
            this.sprite1 = this.add.sprite(150, 380, 'decorations', [0]);
            this.sprite2 = this.add.sprite(190, 215, 'decorations', [1]);
            this.sprite3 = this.add.sprite(218, 210, 'decorations', [2]);
            this.sprite4 = this.add.sprite(755, 245, 'decorations', [3]);
            this.sprite5 = this.add.sprite(510, 150, 'decorations', [4]);

            //Adiciona ao grupo as decorações
            this.group.add(this.sprite1);
            this.group.add(this.sprite2);
            this.group.add(this.sprite3);
            this.group.add(this.sprite4);
            this.group.add(this.sprite5);

        } else if (this.level == 2) {
            //Cria o ground e as plataformas do mapa
            this.platforms.create(480, 420, 'plat2x1');
            this.platforms.create(230, 200, 'plat2x1');
            this.platforms.create(420, 250, 'plat2x1');
            this.platforms.create(680, 300, 'plat2x1');

            //Adiciona as decorações ao mapa
            this.sprite1 = this.add.sprite(455, 380, 'decorations', [0]);
            this.sprite2 = this.add.sprite(210, 160, 'decorations', [1]);
            this.sprite3 = this.add.sprite(235, 162, 'decorations', [2]);
            this.sprite4 = this.add.sprite(400, 210, 'decorations', [3]);
            this.sprite5 = this.add.sprite(690, 261, 'decorations', [4]);

            //Adiciona ao grupo as decorações
            this.group.add(this.sprite1);
            this.group.add(this.sprite2);
            this.group.add(this.sprite3);
            this.group.add(this.sprite4);
            this.group.add(this.sprite5);

        } else if (this.level == 3) {
            //Cria o ground e as plataformas do mapa
            this.platforms.create(40, 150, 'plat2x1');
            this.platforms.create(250, 425, 'plat6x1');
            this.platforms.create(500, 320, 'plat4x1');
            this.platforms.create(230, 230, 'plat1x1');
            this.platforms.create(750, 190, 'plat2x1');

            //Adiciona as decorações ao mapa
            this.sprite1 = this.add.sprite(455, 280, 'decorations', [0]);
            this.sprite2 = this.add.sprite(210, 387, 'decorations', [1]);
            this.sprite3 = this.add.sprite(235, 385, 'decorations', [2]);
            this.sprite4 = this.add.sprite(50, 112, 'decorations', [3]);
            this.sprite5 = this.add.sprite(730, 150, 'decorations', [4]);
            this.sprite6 = this.add.sprite(520, 282, 'decorations', [1]);

            //Adiciona ao grupo as decorações
            this.group.add(this.sprite1);
            this.group.add(this.sprite2);
            this.group.add(this.sprite3);
            this.group.add(this.sprite4);
            this.group.add(this.sprite5);
            this.group.add(this.sprite6);
        }
    }

    //Função para criar o Player
    createPlayer() {
        //Cria um novo objeto com as propriedades fisicas
        this.player = this.physics.add.sprite(100, 450, 'dude');
        //Define que o player faz um pequeno impulso mal toca no solo
        //this.player.setBounce(0.2);
        //Define a área de contacto do player
        this.player.setCircle(17, 2, 6);
        //Define que o player colide com o mundo "Area de Jogo"
        this.player.setCollideWorldBounds(true);
        //defini colisão entre player e plataformas
        this.physics.add.collider(this.player, this.platforms);

        //Animação para o player mover se para a esquerda ou direita
        this.anims.create({
            key: 'left',
            frames: this.anims.generateFrameNumbers('dude', {
                start: 0,
                end: 4
            }),
            frameRate: 10,
            repeat: -1
        });
        //Animação para o player muda de lado
        this.anims.create({
            key: 'turn',
            frames: [{
                key: 'dude',
                frame: 5
            }],
            frameRate: 20
        });
        //Animação para o player mover se para a esquerda ou direita
        this.anims.create({
            key: 'right',
            frames: this.anims.generateFrameNumbers('dude', {
                start: 6,
                end: 10
            }),
            frameRate: 10,
            repeat: -1
        });
    }

    //Função para criar as teclas
    createCursor() {
        this.cursors = this.input.keyboard.createCursorKeys();
    }

    //Função para criar estrelas
    createCoins() {
        //Animação para o player mover se para a esquerda ou direita
        this.anims.create({
            key: 'rotate',
            frames: this.anims.generateFrameNumbers('coinAnim', {
                start: 0,
                end: 3
            }),
            frameRate: 9,
            repeat: -1
        });
        //Random para gerar um número de moedas em cada mapa aleatorio
        //this.coins = Phaser.Math.Between(0, 12);
        //Criação do número de moedas pretendido, com um intervalo
        this.coin = this.physics.add.group({
            key: 'coinAnim',
            repeat: 12, //Número de moedas
            setXY: {
                x: 52,
                y: 0,
                stepX: 70
            }
        });

        //Percorre cada moeda e define o seu Bounce, atribui a animação e define o setCircle
        this.coin.children.iterate((child) => {

            child.setBounceY(Phaser.Math.FloatBetween(0.4, 0.8)).play('rotate');
            child.setCircle(12);

        });

        //Define a colisão da coin com as plataformas
        this.physics.add.collider(this.coin, this.platforms);
        //Permitir que player e moeda possam colidir e que o player as conseguir apanhar
        this.physics.add.overlap(this.player, this.coin, this.collectCoin, null, this);
    }

    //Função para criar o score board
    createScore() {
        //Cria uo scoreBoard
        this.scoreBoard = this.physics.add.staticGroup();

        //Adiciona o Icon das moedas
        this.scoreBoard.create(30, 33, 'coinIcon');
        //Define o score inicial como 0
        this.scoreText = this.add.text(50, 16, '0', {
            fontSize: '32px',
            fill: '#fff'
        });
    }

    //Função para colecionar as moedas
    collectCoin(player, coin) {
        //Esconde a moeda quando o player toca nela
        coin.disableBody(true, true);
        let soundCoins = this.sound.add('audio_coin');
        soundCoins.setVolume(0.5);
        soundCoins.play();
        //Aplica as particulas a cada moeda
        this.particles.emitParticleAt(coin.x, coin.y, 50);

        //Aumenta o score +1
        this.score += 1;
        //Define o novo score
        this.scoreText.setText(this.score);

        //Define se o numero de moedas ativas é 0
        if (this.coin.countActive(true) === 0) {
            //Atualiza as plataformas
            this.updatePlatforms();
            //Percorre as moedas e ativa de novo
            this.coin.children.iterate((child) => {

                child.enableBody(true, child.x, 0, true, true);

            });

            //Determinar o valor de x consoante a posição do player
            const x = (this.player.x < 400) ? Phaser.Math.Between(400, 800) : Phaser.Math.Between(0, 400);

            //Cria os eminigos definindo o setCircle, setBounce, colisao com o mundo e a velocidade
            const enemie = this.enemies.create(x, 100, 'enemies');
            enemie.setCircle(15, 8);
            enemie.setBounce(1);
            enemie.setCollideWorldBounds(true);
            enemie.setVelocity(Phaser.Math.Between(-300, 300), 200);
        }
    }

    //Funcao para criar os inimigos
    createEnemies() {
        //Associa a enemies as propriedades fisicas de um grupo
        this.enemies = this.physics.add.group();
        //Define a colisao dos inimigos com as plataformas
        this.physics.add.collider(this.enemies, this.platforms);
        //Permitir que player e os inimigos possam colidir e que o player morra
        this.physics.add.collider(this.player, this.enemies, this.hitEnemies, null, this);

        //Determinar o valor de x consoante a posição do player
        const x = (this.player.x < 400) ? Phaser.Math.Between(400, 800) : Phaser.Math.Between(0, 400);

        //Cria os eminigos definindo o setCircle, setBounce, colisao com o mundo e a velocidade
        const enemie = this.enemies.create(x, 100, 'enemies');
        enemie.setCircle(15, 8);
        enemie.setBounce(1);
        enemie.setCollideWorldBounds(true);
        enemie.setVelocity(Phaser.Math.Between(-300, 300), 200);

        const enemie2 = this.enemies.create(x, 200, 'enemies');
        enemie2.setCircle(15, 8);
        enemie2.setBounce(1);
        enemie2.setCollideWorldBounds(true);
        enemie2.setVelocity(Phaser.Math.Between(-300, 300), 200);
    }

    //Função para quando o player toca no inimigo
    hitEnemies(player, enemies) {
        let soundhitenemy = this.sound.add('audio_hitenemy');
        soundhitenemy.setVolume(1);
        soundhitenemy.play();
        this.soundBack.stop();

        //Pausa as propriedades fisicas
        this.physics.pause();
        //Define a cor do player como vermelho
        this.player.setTint(0xff0000);
        //Define que o gameOver como true
        this.gameOver = true;
        //Mostrar texto de game over 
        this.gameOverText.visible = true;
        //Score final
        this.finalScoreText = this.add.text(500, 360, 'Score: ' + this.score, {
            fontSize: '44px',
            fill: '#FFD700'
        });
        this.finalScoreText.setOrigin(0.5);
        this.finalScoreText.visible = true;
        //Define o pointerdown para restart
        this.input.on('pointerdown', () => {
            this.scene.start('preload');
            soundBack.play();
            soundBack.setLoop(true);
        });
    }

    //Função para criar as decorações
    createDecorations() {
        //Define um grupo estatico
        this.dec = this.physics.add.staticGroup();

        this.dec = this.add.sprite(300, 520, 'decorations', [4]);
        this.dec = this.add.sprite(330, 520, 'decorations', [0]);
        this.dec = this.add.sprite(650, 520, 'decorations', [2]);
        this.dec = this.add.sprite(680, 520, 'decorations', [3]);
    }

    //Função para update das scenes
    update() {
        this.updatePlayer();
    }

    //Função para dar update ao Player
    updatePlayer() {
        //Caso carregue na seta da esquerda
        if (this.cursors.left.isDown) {
            //define que o player sem move no sentido de x negativo
            this.player.setVelocityX(-150);
            //usa as animações quando se move para a esquerda
            this.player.anims.play('left', true);
        }
        //Caso carregue na seta da direita
        else if (this.cursors.right.isDown) {
            //define que o player sem move no sentido de x positivo
            this.player.setVelocityX(150);
            //usa as animações quando se move para a direita
            this.player.anims.play('right', true);
        } else if (this.cursors.down.isDown) {
            //define que o player sem move no sentido de x positivo
            this.player.setVelocityY(200);
            //usa as animações quando se move para a direita
            this.player.anims.play('turn', true);
        } else {
            //define que o player não se move
            this.player.setVelocityX(0);
            //usa as animações quando esta parado
            this.player.anims.play('turn');
        }
        //Caso carregue na seta para cima e o player esteja a tocar noutro objeto
        if (this.cursors.up.isDown && this.player.body.touching.down) {
            this.soundJump = this.sound.add('audio_jump');
            this.soundJump.setVolume(0.5);
            this.soundJump.play();
            //Define a amplitude y que o player pode saltar
            this.player.setVelocityY(-300);
        }
    }

    //Função para update das plataformas
    updatePlatforms() {
        //Limpa as plataformas do mapa
        this.platforms.clear(true, true);

        //this.decorations.setActive(false).setVisible(false);
        //this.decorations.destroy();
        this.group.killAndHide(this.sprite1);
        this.group.killAndHide(this.sprite2);
        this.group.killAndHide(this.sprite3);
        this.group.killAndHide(this.sprite4);
        this.group.killAndHide(this.sprite5);
        this.group.killAndHide(this.sprite6);

        //Cria um grupo com as propriedades fisicas defenidas e associa as plataformas
        this.platforms = this.physics.add.staticGroup();
        //Faz um random para determinar o nivel inicial
        this.level = Phaser.Math.Between(0, 3);
        //Cria o solo do jogo
        this.platforms.create(480, 580, 'ground').setScale(1, 1).refreshBody();

        //Define um grupo estatico
        this.decorations = this.physics.add.staticGroup();

        //Ciclo para determinar qual o nível que vai começar
        if (this.level == 0) {
            //Cria o ground e as plataformas do mapa
            this.platforms.create(480, 420, 'plat4x1');
            this.platforms.create(230, 200, 'plat2x1');
            this.platforms.create(420, 250, 'plat1x1');
            this.platforms.create(680, 300, 'plat4x1');

            //Adiciona as decorações ao mapa
            this.sprite1 = this.add.sprite(430, 380, 'decorations', [0]);
            this.sprite2 = this.add.sprite(460, 380, 'decorations', [4]);
            this.sprite3 = this.add.sprite(710, 262, 'decorations', [2]);
            this.sprite4 = this.add.sprite(740, 262, 'decorations', [3]);
            this.sprite5 = this.add.sprite(250, 160, 'decorations', [1]);
            //Adiciona ao grupo as decorações
            this.group.add(this.sprite1);
            this.group.add(this.sprite2);
            this.group.add(this.sprite3);
            this.group.add(this.sprite4);
            this.group.add(this.sprite5);

        } else if (this.level == 1) {
            //Cria o ground e as plataformas do mapa
            this.platforms.create(120, 420, 'plat6x1');
            this.platforms.create(410, 340, 'plat2x1');
            this.platforms.create(588, 517, 'plat1x1');
            this.platforms.create(176, 252, 'plat4x1');
            this.platforms.create(472, 188, 'plat4x1');
            this.platforms.create(778, 284, 'plat2x1');

            //Adiciona as decorações ao mapa
            this.sprite1 = this.add.sprite(150, 380, 'decorations', [0]);
            this.sprite2 = this.add.sprite(190, 215, 'decorations', [1]);
            this.sprite3 = this.add.sprite(218, 210, 'decorations', [2]);
            this.sprite4 = this.add.sprite(755, 245, 'decorations', [3]);
            this.sprite5 = this.add.sprite(510, 150, 'decorations', [4]);

            //Adiciona ao grupo as decorações
            this.group.add(this.sprite1);
            this.group.add(this.sprite2);
            this.group.add(this.sprite3);
            this.group.add(this.sprite4);
            this.group.add(this.sprite5);

        } else if (this.level == 2) {
            //Cria o ground e as plataformas do mapa
            this.platforms.create(480, 420, 'plat2x1');
            this.platforms.create(230, 200, 'plat2x1');
            this.platforms.create(420, 250, 'plat2x1');
            this.platforms.create(680, 300, 'plat2x1');

            //Adiciona as decorações ao mapa
            this.sprite1 = this.add.sprite(455, 380, 'decorations', [0]);
            this.sprite2 = this.add.sprite(210, 160, 'decorations', [1]);
            this.sprite3 = this.add.sprite(235, 162, 'decorations', [2]);
            this.sprite4 = this.add.sprite(400, 210, 'decorations', [3]);
            this.sprite5 = this.add.sprite(690, 261, 'decorations', [4]);

            //Adiciona ao grupo as decorações
            this.group.add(this.sprite1);
            this.group.add(this.sprite2);
            this.group.add(this.sprite3);
            this.group.add(this.sprite4);
            this.group.add(this.sprite5);

        } else if (this.level == 3) {
            //Cria o ground e as plataformas do mapa
            this.platforms.create(40, 150, 'plat2x1');
            this.platforms.create(250, 425, 'plat6x1');
            this.platforms.create(500, 320, 'plat4x1');
            this.platforms.create(230, 230, 'plat1x1');
            this.platforms.create(750, 190, 'plat2x1');

            //Adiciona as decorações ao mapa
            this.sprite1 = this.add.sprite(455, 280, 'decorations', [0]);
            this.sprite2 = this.add.sprite(210, 387, 'decorations', [1]);
            this.sprite3 = this.add.sprite(235, 385, 'decorations', [2]);
            this.sprite4 = this.add.sprite(50, 112, 'decorations', [3]);
            this.sprite5 = this.add.sprite(730, 150, 'decorations', [4]);
            this.sprite6 = this.add.sprite(520, 282, 'decorations', [1]);

            //Adiciona ao grupo as decorações
            this.group.add(this.sprite1);
            this.group.add(this.sprite2);
            this.group.add(this.sprite3);
            this.group.add(this.sprite4);
            this.group.add(this.sprite5);
            this.group.add(this.sprite6);
        }

        //Define a colisao dos inimigos com as plataformas
        this.physics.add.collider(this.enemies, this.platforms);
        //Permitir que player e os inimigos possam colidir e que o player morra
        this.physics.add.collider(this.player, this.enemies, this.hitEnemies, null, this);
        //defini colisão entre player e plataformas
        this.physics.add.collider(this.player, this.platforms);
        //defini colisão entre coin e plataformas
        this.physics.add.collider(this.coin, this.platforms);
        //Permitir que player e moeda possam colidir e que o player as conseguir apanhar
        this.physics.add.overlap(this.player, this.coin, this.collectCoin, null, this);
    }
}

export default GameScene